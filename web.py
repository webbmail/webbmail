#!/usr/bin/env python
# Copyright (c) 2010 Ben Webb <bjwebb@libreapps.com>
# Released as free software under the MIT license,
# see the LICENSE file for details.
import cherrypy
import os
import emaillib
from genshi.template import TemplateLoader
from genshi.input import HTML
from genshi.filters import HTMLFormFiller, HTMLSanitizer
from genshi.core import Markup

def get_nice_body(doc):
    if "body" in doc:
        return Markup("<pre>") + doc["body"] + Markup("</pre>")
    else:
        return ""

class EmailWeb(object):
    regfail = False
    
    def __init__(self):
        self.loader = TemplateLoader(
            os.path.join(os.path.dirname(__file__), 'templates'),
            auto_reload=True
        )
    
    def register(self, **args):
        if args["password"] != args["password2"]:
            self.regfail = True
            return self.index()
        emaillib.create_user(args["username"]+"@xn--gce.com", args["password"])
        return self.index(**args)
    
    def index(self, **args):
        fail = False
        if "logout" in args:
            cherrypy.session.clear()
        elif "username" in args:
            myaddr = args["username"]+"@xn--gce.com"
            if emaillib.check_password(myaddr, args["password"]):
                cherrypy.session["username"] = myaddr
                raise cherrypy.HTTPRedirect("list")
            else:
                fail = True
        tmpl = self.loader.load('index.html')
        return tmpl.generate(fail=fail, regfail=self.regfail).render('html', doctype='html')

    def list(self, **args):
        myaddr = self.user_stuff()
        rows = emaillib.get_list(myaddr)
        tmpl = self.loader.load('list.html')
        return tmpl.generate(user=myaddr, rows=rows).render('html', doctype='html')
    
    def convo(self, **args):
        myaddr = self.user_stuff()
        emails = emaillib.get_convo(myaddr, args["id"])
        tmpl = self.loader.load('convo.html')
        def bodymod(docs):
            for doc in docs:
                print doc
                doc["_nicebody"] = get_nice_body(doc)
                yield doc
        return tmpl.generate(
            user=myaddr, emails=bodymod(emails),
            shown_headers=["To","From","Subject"]
            ).render('html', doctype='html')
    
    def mail(self, **args):
        myaddr = self.user_stuff()
        doc = emaillib.get_mail(myaddr, args["id"])
        tmpl = self.loader.load('mail.html')
        body = get_nice_body(doc)
        return tmpl.generate(user=myaddr, email=doc, body=body).render('html', doctype='html')
    
    def compose(self):
        return """<form><textarea name="message"></textarea></form>"""
    
    def admin(self):
        out = ""
        for u in emaillib.list_users():
            out += u["user"]+"<br/>"
        return out
    
    def user_stuff(self):
        if "username" in cherrypy.session:
            return cherrypy.session["username"]
        else:
            raise cherrypy.HTTPRedirect("index")
    
    register.exposed = True
    index.exposed = True
    list.exposed = True
    mail.exposed = True
    convo.exposed = True
    compose.exposed = True
    admin.exposed = True


config = {
    "global": {
        "server.socket_port": 8081
    },
    "/": {
        "tools.sessions.on": True,
        "tools.sessions.storage_type": "file",
        "tools.sessions.storage_path": "/home/bjwebb/sessions",
        "tools.sessions.timeout": 60,
        "tools.staticdir.root": "/home/bjwebb"
    },
    "/static": {
        "tools.staticdir.on": True,
        "tools.staticdir.dir": "static"
    }
}

cherrypy.quickstart(EmailWeb(), config=config)
