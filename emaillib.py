#!/usr/bin/env python
# Copyright (c) 2010 Ben Webb <bjwebb@libreapps.com>
# Released as free software under the MIT license,
# see the LICENSE file for details.
import couchdb
import hashlib
import random

couch = couchdb.Server("http://127.0.0.1:5984/")
db = couch["mail"]

def check_password(user, password):
    map_fun = """function(doc) {
        if (doc.type == "user" && doc.user) {
            emit(doc.user, doc);
        }
    }"""
    
    results = db.query(map_fun, key=user)
    for result in results:
        doc = result["value"]
        return "value" in result and verify_hash(password, doc["password"])


def verify_hash(text, storedhash):
    salt = storedhash[0:10]
    return storedhash == hash(text, salt)

def hash(text, salt=None):
    if salt == None:
        salt = ""
        for i in range(0,10):
            salt += random.choice("0123456789abcdef")
    return salt+hashlib.sha1(salt+text).hexdigest()

def get_message_list(user):
    map_fun = """function(doc) {
        if (doc.type == "email" && doc.user) {
            emit(doc.user, doc);
        }
    }"""
    
    results = db.query(map_fun, key=user)
    for result in results:
        doc = result["value"]
        yield doc
        
def get_list(user):
    map_fun = """function(doc) {
        if (doc.type == "email" && doc.user == '"""+user+"""') {
            emit(doc.convo, [1, {msgid:doc._id, date:doc.headers["Date"][0], subject:doc.headers["Subject"][0]}]);
        }
    }"""
    
    reduce_fun = """function(tag, keys) {
        log(keys)
        var sum = 0;
        var t = "";
        var msg = ""
        for(var i = 0; i < keys.length; i++) {
            sum += keys[i][0];
            if (msg == "" || keys[i][1]["date"] < msg["date"]) {
                msg = keys[i][1]
            }
        };
        var test = [sum, msg];
        return test;
    }"""
    
    results = db.query(map_fun, reduce_fun, group=True)
    for result in results:
        r = result["value"][1]
        r["id"] = result["key"]
        r["count"] = result["value"][0]
        yield r

def get_mail(user, id):
    map_fun = """function(doc) {
        if (doc.type == "email") {
            emit(doc._id, doc);
        }
    }"""
    
    results = db.query(map_fun, key=id)
    for result in results:
        doc = result["value"]
        if doc["user"] == user:
            doc = result["value"]
            return doc


def get_convo(user, id):
    map_fun = """function(doc) {
        if (doc.type == "email") {
            emit(doc.convo, doc);
        }
    }"""
    
    results = db.query(map_fun, key=id)
    for result in results:
        doc = result["value"]
        if doc["user"] == user:
            yield doc

def create_user(user, password):
    from uuid import uuid4
    id = uuid4().hex
    db[id] = {
        "type": "user",
        "user": user,
        "password": hash(password)
    }

def list_users():
    map_fun = """function(doc) {
        if (doc.type == "user") {
            emit(doc.user, doc);
        }
    }"""
    
    results = db.query(map_fun)
    for result in results:
        yield result["value"]



from zope.interface import implements#, Interface, Attribute
from twisted.internet import defer
from twisted.python import failure#, log
from twisted.cred import checkers, error, credentials
class CouchDBPasswordDatabase:
    implements(checkers.ICredentialsChecker)

    credentialInterfaces = (credentials.IUsernamePassword,
                            credentials.IUsernameHashedPassword)

    def __init__(self, **users):
        self.users = users

    """def addUser(self, username, password):
        self.users[username] = password"""

    def _cbPasswordMatch(self, matched, username):
        if matched:
            return username
        else:
            return failure.Failure(error.UnauthorizedLogin())

    def fixme(self):
        return True

    def requestAvatarId(self, credentials):
        if check_password(credentials.username, credentials.password):
            return defer.maybeDeferred(self.fixme).addCallback(
                self._cbPasswordMatch, str(credentials.username))
        else:
            return defer.fail(error.UnauthorizedLogin())


"""
    implements(checkers.ICredentialsChecker)

    credentialInterfaces = (credentials.IUsernamePassword)#,
                            #credentials.IUsernameHashedPassword)

    def __init__(self):
        print "TEST0"

    def checkPassword(self, password):
        print "TEST"

    def providedBy(self, smt):
        print "TESTA"

    def requestAvatarId(self, credentials):
        print "TEST2"
        if check_password(credentials.username, crendentials.password):
            return str(credentials.username)
        else:
            return failure.Failure(error.UnauthorizedLogin())
"""


