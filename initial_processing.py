#!/usr/bin/env python
# Copyright (c) 2010 Ben Webb <bjwebb67@libreapps.com>
# Released as free software under the MIT license,
# see the LICENSE file for details.
import sys
import couchdb

couch = couchdb.Server("http://127.0.0.1:5984/")
#couch = couchdb.Server()
db = couch["mail"]

all = sys.stdin.read()

doc = {"all": all}
if len(sys.argv) >= 2:
    doc["user"] = sys.argv[1]
import email.parser
parser = email.parser.Parser()
msg = parser.parsestr(doc["all"])
headers = {}
for k,v in msg.items():
    if not k in headers:
        headers[k] = []
    headers[k].append(v)
doc["headers"] = headers

doc["type"] = "email"
if msg.is_multipart():
    doc["body"] = "Multipart"
else:
    doc["body"] = msg.get_payload()

##### Assign the message to a conversation #####
if "References" in doc["headers"]:
    refaddrs = re.findall("\<.*?\>", str(doc["headers"]["References"]))
else:
    refaddrs = {}
done = False
for refaddr in refaddrs:
    print refaddr
    map_fun = """function(doc) {
        if (doc.type == "email" && doc.user && doc.headers["Message-ID"]) {
            emit([doc.user, doc.headers["Message-ID"][0]], doc);
        }
    }"""
    results = db.query(map_fun, key=[doc["user"], str(refaddr)])
    for result in results:
        print "Test"
        doc2 = result["value"]
        if "convo" in doc2:
            doc["convo"] = doc2["convo"]
        else:
            from uuid import uuid4
            convo_id = uuid4().hex
            doc["convo"] = convo_id
            doc2["convo"] = convo_id
            db.update([doc2])
        done = True
        break
    if done:
        break
if not done:        
    from uuid import uuid4
    convo_id = uuid4().hex
    doc["convo"] = convo_id


from uuid import uuid4
doc_id = uuid4().hex
db[doc_id] = doc

print all

